import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;





	
public class main1 {
	interface worker1<M,N,R>
	{
		public R do1(M data,N data1);
	}

interface worker2<M,R>
{
	public R do1(M data);
}
	
	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws IOException
	{
		Stream<String> stream=Files.lines(Paths.get("in"));
			List<MonitoredData> infos=stream.map(aux->{
				MonitoredData nouve=new MonitoredData();
				//aux.trim();
				String []tokens=aux.split("[\\t+\\s+\\-\\:]");
				
				Date start= new Date();
				start.setYear(Integer.parseInt(tokens[0]));
				start.setMonth(Integer.parseInt(tokens[1]));
				start.setDate(Integer.parseInt(tokens[2]));
				//System.out.println(Integer.parseInt(tokens[2]));
				start.setHours(Integer.parseInt(tokens[3]));
				start.setMinutes(Integer.parseInt(tokens[4]));
				start.setSeconds(Integer.parseInt(tokens[5]));
				
				nouve.setStart(start);
				
				Date stop= new Date();
				stop.setYear(Integer.parseInt(tokens[7]));
				stop.setMonth(Integer.parseInt(tokens[8]));
				stop.setDate(Integer.parseInt(tokens[9]));
				stop.setHours(Integer.parseInt(tokens[10]));
				stop.setMinutes(Integer.parseInt(tokens[11]));
				stop.setSeconds(Integer.parseInt(tokens[12]));
				
				nouve.setStop(stop);
				nouve.setName(tokens[14]);
				return nouve;
				
			}
					).collect(Collectors.toList());
			
		 PrintWriter scaner2=new PrintWriter(new File("pb2.txt"));
		 PrintWriter scaner3=new PrintWriter(new File("pb3.txt"));
		 PrintWriter scaner4=new PrintWriter(new File("pb4.txt"));
		 PrintWriter scaner5=new PrintWriter(new File("pb5.txt"));
			long p1=infos.stream().map(a->a.getStart().getDate()).distinct().count();
			System.out.println(p1);			
			Map<String,Long> pb2=infos.stream().collect(Collectors.groupingBy(e->e.getName(),Collectors.counting()));
			pb2.forEach((key,value)->
			{
				scaner2.println(key+value);
			});
			scaner2.close();

			Map<Integer,Map<String,Long>> pb3=infos.stream().collect(Collectors
					.groupingBy(e->e.getStart()
							.getDate(),Collectors
							.groupingBy(e->e.getName(),Collectors.counting())));
			pb3.forEach((key,value)->
			{
				for(String aux:value.keySet())
				{
					scaner3.println(key+" "+aux+" "+value.get(aux));
				}
			});
			scaner3.close();
			Map<String,Long> pb4=infos.stream().collect(Collectors.groupingBy(e->e.getName(),Collectors.summingLong(e->
			{
				long diffInMillies = -e.getStart().getTime() + e.getStop().getTime();
				return diffInMillies/(1000*3600);
				
			})))
			;
	
//			for(String aux:pb4.keySet())
//			{
//				System.out.println(aux+" "+pb4.get(aux));
//			}
			System.out.println("\n\n\n");
			pb4=pb4.entrySet().stream().filter(t->t.getValue()>10).collect(Collectors.toMap(t->t.getKey(),t->t.getValue()));
			pb4.forEach((key,value)->
			{
				
					scaner4.println(key+" "+value);
				
			});
			scaner4.close();
			Map<String,Long> pb51=infos.stream().filter(t->
			{
				if((t.getStop().getTime()-t.getStart().getTime())/(1000*3600)<5)
					return true;
				else
					return false;
				
			}).
			
			
			collect(Collectors.groupingBy(t->
				t.getName()
				
					
			,Collectors.counting()));
			Map<String,Long> pb52=infos.stream().	
			collect(Collectors.groupingBy(t->
				t.getName()
				
					
			,Collectors.counting()));
			pb51.entrySet().stream().filter(t->{
				if(t.getValue()/pb52.get(t.getKey())>0.9)
				{
					return true;
				}
				else
				{
					return false;
				}
			}).forEach(t->{
			scaner5.println(t.getKey()+" "+t.getValue());
			}
					);
			
			scaner5.close();
			
			
			
			
	}
}

		
//	